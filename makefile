

# Simple makefile loading the main file, using sbcl. Similar steps can be taken for other lisps
FLAGS=--output run/tis99 --asdf-path . --load-system tis-99 --entry tis-99:main
SBCL_FLAGS=--noinform --no-userinit --no-sysinit \
					 --eval "(require 'asdf)" \
					 --eval "(push (truename \".\") asdf:*central-registry*)" \
					 --eval "(require :tis-99)"
NONINTERACTIVE=--non-interactive

RUN_FLAG=--eval "(tis-99:main)"
IN_PKG=--eval "(in-package :tis-99)"
QUIT_FLAG=--eval "(quit)"

all:
	sbcl $(SBCL_FLAGS) $(NONINTERACTIVE) $(QUIT_FLAG)

dir:
	mkdir -p run

run:
	sbcl $(SBCL_FLAGS) $(RUN_FLAG) $(NONINTERACTIVE) $(QUIT_FLAG)

debug:
	sbcl $(SBCL_FLAGS) --eval "(declaim (optimize (debug 3)))" $(IN_PKG)

release:dir
	buildapp --compress-core $(FLAGS)


clean:
	-rm -rf ./run
	-find . -name '*.fasl' | xargs rm -f

alt:
	sbcl --load build.lisp --name tis-99

win:
	wine sbcl --load build.lisp --name tis-99


.PHONY: run
