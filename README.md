# TIS-99 [![Circle CI](https://circleci.com/gh/jgkamat/TIS-99/tree/master.svg?style=svg)](https://circleci.com/gh/jgkamat/TIS-99/tree/master)

TIS-99's spec was created before the invention of the massively popular TIS-100, but due to budget and feasibility constraints, the spec was massively scaled back to accommodate the technology at the time, and the TIS-99 was forgotten. Now, with recent developments in high level languages, processor design, and operating systems, the original design of of TIS-99 can become a reality, and run on any modern platform!

## Running

To run on *nix, simply head on over to the [releases page](https://github.com/jgkamat/TIS-99/releases) and download the release file. Then set the file as executable and run it.

## Building

TIS-99 is made in common lisp, which is a demonstration of the newest ideas in programming language design! Implementing TIS-99 in common lisp will future proof it for generations to come.

You can run TIS-99 on any common unix system by running `make run` in the project directory. This will produce an executable in `./run/tis99`. This requires buildapp and sbcl or ccl.

```
sudo apt-get install buildapp sbcl # On debian
make run
```

Alternatively, you can load the TIS-99 asdf project found in `tis-99.asd` and run `(tis-99:main)`, found in `src/tis99.lisp`.

# Development

The TIS-99 specification has already been fully released by TIS; this project is an effort to create it as free software, to make it more widely available to the common hacker. Help create and extend the TIS-99 spec!

## Type T21+

### Single Node Emulation

Single node emulation for the T21+ is complete! Support is currently available for NOP, MOV, SWP, SAV, ADD, SUB, NEG, JMP, JEZ, JNZ, JGZ, JLZ, and JRO commands from the TIS-100. In addition, in accordance with the TIS-99 spec, the following commands have been added.

| Command    | Function                   |
|----------- |----------------------------|
| `SHW`, `LS`| Print node state to STDOUT |
| `MUL <LOC>`| Multiply ACC by `<LOC>`    |
| `DIV <LOC>`| Divide ACC by `<LOC>`      |
| `PRG`      | Print out program so far   |
| `QUIT`     | Quit to lisp interpreter   |

In addition, the PC register has been added. You can issue commands such as `mov ACC, PC` to execute instruction number `ACC` next. See the output of `PRG` for instruction numbers.

### Multi Node Emulation

Multi Node emulation is planned to follow the TIS-99 spec, of using UNIX pipes for communication between nodes, allowing third party programs to be linked to parts of the TIS-99 for extensibility!
