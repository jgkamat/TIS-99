
;;;; A Simple accessor class that is shared everywhere. It just has a getter and a setter function.

(in-package #:tis-99)

;;; Code

(defclass accessor ()
  ((getter
     :initarg :getter
     :initform (error "No getter supplied.")
     :reader getter
     :documentation "Getter function for this type.")
    (setter
      :initarg :setter
      :initform (error "No setter supplied.")
      :reader setter
      :documentation "Getter function for this type."))
 (:documentation "A simple accessor class containing getter and setter functions."))
