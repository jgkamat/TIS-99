
;;;; Pipe classes/methods

;;; Packages
(in-package #:tis-99)

;;; Code
(deftype pipe-mode () '(mkfifo variables)
  "An Enumerated type describing pipe modes")

(defclass pipe ()
  ((mode
     :initarg :mode
     :initform 'variables
     :reader pmode
     :type pipe-mode
     :documentation "Whether this pipe is in backup mode or in fifo mode.")
    (accessor
      :initform nil
      :reader pipe-accessor
      :documentation "Method for getting/setting to this virutal pipe.")
    (buffer
      :initform nil
      :accessor pipe-buffer
      :type (integer)
      :documentation "The buffer that set variables go to before a tick happens (at which point they can be read")
    (bup-buffer
      :initform nil
      :accessor pipe-backup-buffer
      :type (integer)
      :documentation "The backup output buffer to use when in variable mode (not mkfifo mdoe).")
    (blocked
      :initform nil
      :accessor pipe-blocked
      :documentation "Whether this pipe is blocked and waiting for a read.")
    (unblock-list
      :initform '()
      :accessor pipe-unblock
      :type (list)
      :documentation "A list of nodes to unblock when this pipe gets cleared. Will clear this var whenever an unblock occurs."))
  (:documentation "A Pipe Abstraction, to facilitate connecting nodes."))

(defun pipe-tick (pipe)
  "Ticks a pipe, flushing the input buffer and placing it's number in the output buffer."
  (unless (pipe-blocked pipe)
    (setf (pipe-blocked pipe) t)
    (if (eql (pmode pipe) 'variables)
      (progn
        (rotatef (pipe-buffer pipe) (pipe-backup-buffer pipe))
        (setf (pipe-buffer pipe) nil))
      (error "Not implemented yet."))))

(defmethod initialize-instance :after ((pipe pipe) &key)
  "Adds the accessor method for this pipe, with a closure around this pipe object."
  (unless (slot-value pipe 'accessor)
    (setf (slot-value pipe 'accessor)
      (make-instance 'accessor
        ;; <3 closures!
        :getter #'(lambda ()
                    (if (eql (pmode pipe) 'variables)
                      (progn
                        (pipe-backup-buffer pipe)
                        (dolist (x (pipe-unblock pipe))
                          (setf (node-blocked x) nil)))
                      (error "Not implemented yet."))
                    (setf (pipe-unblock pipe) '())
                    (setf (pipe-blocked pipe) nil))
        :setter #'(lambda (val &optional node)
                    (when node
                      (push node (pipe-unblock pipe)))
                    (if (eql (pmode pipe) 'variables)
                      (setf  (pipe-buffer pipe) val)
                      (error "Not implemented yet.")))))))
