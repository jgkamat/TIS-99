
(in-package #:tis-99)

;;; Code
(defclass t21+ (tis-node)
  ((bak
      :initarg :bak
      :initform 0
      :accessor t21-bak
      :documentation "The BAK register for the T21+ node."))
  (:documentation "A T21+ Node Class."))


(defmethod SWP ((node t21+) (param list))
  (rotatef (t21-acc node) (t21-bak node)))

(defmethod SAV ((node t21+) (param list))
  (setf (t21-bak node) (t21-acc node)))
