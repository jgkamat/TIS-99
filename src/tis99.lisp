
;;;; Main code file for TIS-99

;;; Packages
(in-package #:tis-99)

;; Initialize registers to 0
(defparameter *MAIN-NODE*
  (make-instance 't21+
    :name "main node")
  "The default node")

(defparameter *QUIT* nil
  "If quit is t, then exit the program")

(defparameter *COMMANDS*
  (make-hash-table :test #'EQUALP)
  "Commands hash table, contains links from command names to functions.
Global between all nodes.")

(defun main (&rest args)
  (handler-case
    (progn
      "Main method, use to start the TIS-99 Shell"
      ;; TODO use args
      args

      (let ((node *MAIN-NODE*))
        (setf *QUIT* nil)

        ;; Make sure User commands always run
        (setf (t21-pc node) (length (t21-prog node)))

        (format t "----------------------TIS-99 SHELL----------------------~%")
        (format t "COPYRIGHT JAY KAMAT 2016: GNU GENERAL PURPOSE LICENSE v3~%")
        (loop
          (when *QUIT* (return *QUIT*))
          ;; (print *PROG*)
          (if (< (t21-pc node) (length (t21-prog node)))
            ;; Run stored code if it exists already
            (run-cmd node (elt (t21-prog node) (t21-pc node)))
            ;; Take input from user (and strip comments)
            (let ((prompt (split-on (remove-comment (prompt-read) #\#))))
              ;; If an error occurs, do not add it to history!
              (handler-case
                (progn
                  (setf prompt (parse-labels node prompt))
                  (setf prompt (parse-prompt node prompt))
                  (run-cmd node prompt)
                  (add-cmd-history node prompt))
                (tis-parse-error (x) (format *error-output* "~a" (text x))))))
          ;; Increment the PC regardless
          (incf (t21-pc node)))))
    (sb-sys:interactive-interrupt ())))
