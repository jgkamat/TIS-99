
;;;; A file with all string processing functions

(in-package #:tis-99)


;;; String processing
(defun split-on (string &optional (token #\SPACE))
  "A recursive function to split strings into lists of strings by a token"

  ;; Turn commas into spaces
  (setf string (substitute #\SPACE #\, string))

  ;; Trim incoming strings by the token
  (setf string (string-trim (list token) string))

  (let ((pos (position token string)))
    (if pos
      (append
        (list (string-trim (list token) (subseq string 0 pos)))
        (split-on (subseq string (1+ pos)) token))
      (list string))))


;;; Removes parts of a line after a comment (comment char)
(defun remove-comment (str-var &optional (comment-char #\#))
  (let ((string str-var))
    (subseq string 0 (position comment-char string))))
