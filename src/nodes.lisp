

;;;; Nodes for the TIS-99

;;; Packages
(in-package #:tis-99)

;;; Code

(defclass accessor ()
  ((getter
     :initarg :getter
     :initform (error "No getter supplied.")
     :reader getter
     :documentation "Getter function for this type.")
    (setter
      :initarg :setter
      :initform (error "No setter supplied.")
      :reader setter
      :documentation "Getter function for this type."))
 (:documentation "A simple accessor class containing getter and setter functions."))


(defclass tis-node ()
  ((name
     :initarg :name
     :initform (error "Supply a node name")
     :accessor node-name
     :documentation "The name of this node")
    (blocked
      :initform nil
      :accessor node-blocked
      :documentation "Whether this node is blocked on an I/O operation.")
    (pc
      :initarg :pc
      :initform 0
      :accessor t21-pc
      :documentation "The PC variable to index into PROG")
    (labels
      :initarg :labels
      :initform (make-hash-table :test #'EQUALP)
      :accessor t21-labels
      :documentation "Contains the label indexes into PROG")
    (prog
      :initarg :prog
      :initform (make-array 0 :adjustable t :fill-pointer 0)
      :accessor t21-prog
      :documentation "The current instruction vector of this node.")
    (trash
      :initarg :nil
      :initform 0
      :accessor t21-nil
      :documentation "A dummy var to throw away data too.")
    (acc
      :initarg :acc
      :initform 0
      :accessor t21-acc
      :documentation "The ACC register for a tis node.")
    (locations
      :initarg :locations
      :initform (make-hash-table :test #'EQUALP)
      :accessor t21-locations
      :documentation "Locations to index into, such as UP, DOWN, ACC. etc.")
    (commands
      :initarg :commands
      :initform (make-hash-table :test #'EQUALP)
      :accessor tis-commands
      :allocation :class
      :documentation "A list of commands that this node can run"))
  (:documentation "A generic TIS-99 Node, containing I/O Operation things"))

(defmacro add-location (string payload node)
  "Adds the given variable to the string hashmap"
  `(setf (gethash ,string (t21-locations ,node)) ,payload))

(defmacro add-location-auto (string setf-getter node)
  "Adds the given variable to the string hashmap,
and creates an accessor for you using the setf function you provide.

setf-getter must be the value of a setfable function definition, not the actual function."
  `(add-location ,string (make-instance 'accessor
                           :getter #'(lambda () (funcall #',setf-getter node))
                           :setter #'(lambda (x) (setf (,setf-getter node) x)))
     ,node))

(defmethod add-cmd ((node tis-node) string (function function))
  "Adds the given command and string to the hashmap"
  (setf (gethash string (tis-commands node)) function))

(defmethod initialize-instance :after ((node tis-node) &key)
  ;; (add-location "ACC" (make-instance 'accessor
  ;;                       :getter #'t21-acc
  ;;                       :setter #'(lambda (x) (setf (t21-acc node) x))) node)

  ;; TODO make this run even when this is in a superclass
  (when (= (hash-table-count (tis-commands node)) 0)
    ;; Add commands to hashtable
    (add-cmd node "SHW" #'SHW)
    (add-cmd node "LS" #'SHW)
    (add-cmd node "ADD" #'ADD)
    (add-cmd node "SUB" #'SUB)
    (add-cmd node "SWP" #'SWP)
    (add-cmd node "SAV" #'SAV)
    (add-cmd node "MUL" #'MUL)
    (add-cmd node "MULT" #'MUL)
    (add-cmd node "DIV" #'DIV)
    (add-cmd node "MOV" #'MOV)
    (add-cmd node "JMP" #'JMP)
    (add-cmd node "JLZ" #'JLZ)
    (add-cmd node "JGZ" #'JGZ)
    (add-cmd node "JEZ" #'JEZ)
    (add-cmd node "JNZ" #'JNZ)
    (add-cmd node "PRG" #'PRG)
    (add-cmd node "JRO" #'JRO)
    (add-cmd node "NOP" #'NOP)
    (add-cmd node "NEG" #'NEG)
    (add-cmd node "QUIT" #'HCF)
    (add-cmd node "Q" #'HCF)
    (add-cmd node "HCF" #'HCF)
    ;; Run nothing when simply enter is pressed
    (add-cmd node "" #'NOP)
    (add-cmd node nil #'NOP))

  (add-location-auto "ACC" t21-acc node)
  (add-location-auto "NIL" t21-nil node)
  ;; Be careful =]
  (add-location-auto "PC" t21-pc node))


(defmacro num-modify (num mod-func variable)
  "Modifies ACC with a supplied funciton and an int number."
  ;; If num is a sequence, just take the first element
  `(setf ,variable (funcall ,mod-func (eval ,variable) ,num)))

(defmethod setf-multitype ((node tis-node) var var2)
  "Takes in a int or a VAR as arguments, and sets the first var to the second var."
  (let ((toSet (if (integerp var2) var2
                 (funcall (getter var2)))))
    (cond
      ((integerp var)
        nil)
      ((typep var 'accessor)
        (progn
          (setf (t21-nil node) 0)
          (funcall (setter var) toSet)))
      (t (error 'tis-parse-error
           :text (format nil "A non integer was treated as an int!~%"))))))

;; Command Functions
(defmethod MOV ((node tis-node) (parameters list))
  (if (eql (length parameters) 2)
    (setf-multitype node (elt parameters 1) (elt parameters 0))
    (error 'tis-parse-error :text
      (format nil "Incorrect number of arguments~%"))))

(defmethod SHW ((node tis-node) parameters)
  "A function for the SHW command. Takes a list of strings"
  (format t "ACC: ~a~%BAK: ~a~%" (t21-acc node) (t21-bak node)))

(defmethod ADD ((node tis-node) (param list))
  (num-modify (first param) #'+ (t21-acc node)))

(defmethod SUB ((node tis-node) (param list))
  (num-modify (first param) #'- (t21-acc node)))

(defmethod NEG ((node tis-node) (param list))
  (num-modify 0 #'(lambda (x &rest y) y (- x)) (t21-acc node)))

(defmethod MUL ((node tis-node) (param list))
  (num-modify (first param) #'* (t21-acc node)))

(defmethod DIV ((node tis-node) (param list))
  (num-modify (first param) #'(lambda (x y) (floor (/ x y))) (t21-acc node)))

(defmethod JRO ((node tis-node) (param list))
  (num-modify (first param) #'(lambda (x y) (+ x y -1)) (t21-pc node)))

(defun NOP (&rest throwaway)
  (declare (ignore throwaway)))

(defmethod JMP ((node tis-node) (param list))
  "Jumps to the label defined by PARAM"
  (declare (type tis-node node)
    (type list param))
  (if (eql (length param) 1)
    ;; We decrement by 1 as the *PC* will be incremented at the end of this cycle
    (setf (t21-pc node) (1- (first param)))
    (error 'tis-parse-error :text
      (format nil "Incorrect number of arguments~%"))))

(defmethod PRG ((node tis-node) (param list))
  "Prints the program code so far"
  (let ((label-tbl (make-hash-table)))

    ;; make a hashtable reverse to *LABEL*
    ;; This will only print one label per line, multiple labels are supported
    (maphash #'(lambda (k v) (setf (gethash v label-tbl)
                               (concatenate 'string k ":")))
      (t21-labels node))
    ;; Actually print using a Format macro
    (dotimes (num (length (t21-prog node)))
      ;; Only print commas where they should be in assembly
      (format t
        "~5A ~:@(~10A~) ~{~#[~;~:@(~a~)~;~:@(~a~) ~:@(~a~)~:;~:@(~a~) ~@{~:@(~a~)~^, ~}~]~}~%"
        num
        (gethash num label-tbl "")
        (elt (t21-prog node) num)))))

(defmethod HCF ((node tis-node) (param list))
  "A quit function"
  (setf *QUIT* t))

(defmacro gen-jmp (name function)
  "Generates functions like JEZ, JGZ, etc"
  (let ((node-var (gensym))
         (param-var (gensym)))
    `(defmethod ,name ((,node-var tis-node) (,param-var list))
       (if (funcall ,function (t21-acc ,node-var) 0)
         (JMP ,node-var ,param-var)))))
(gen-jmp JEZ #'=)
(gen-jmp JGZ #'>)
(gen-jmp JLZ #'<)
(gen-jmp JNZ #'/=)


(defmethod run-cmd ((node tis-node) (prompt list))
  "Runs a commmand given a full prompt"
  (if (gethash (first prompt) (tis-commands node))
    (funcall (gethash (first prompt) (tis-commands node)) node (rest prompt))
    (error 'tis-parse-error :text
      (format nil "Command not found.~%"))))

(defmethod add-cmd-history ((node tis-node) (prompt list))
  "Adds a command to *PROG* given the full prompt"
  (unless (or (equal prompt '("")) (eql prompt nil))
    (vector-push-extend prompt (t21-prog node))))

(defmethod add-label ((node tis-node) (element string))
  "Takes an element of a prompt, and parses/adds labels. Returns nil if no label found"
  (if (and (> (length element) 1)
        (eql (elt element (1- (length element))) #\:))
    (let ((label (subseq element 0 (1- (length element)))))
      (when (gethash label (t21-locations node))
        (error 'tis-parse-error :text
          (format nil "Label name is reserved.~%")))
      (setf (gethash label (t21-labels node)) (t21-pc node))
      label)
    nil))

(defmethod parse-labels ((node tis-node) (prompt list))
  "Takes in a prompt string, and adds labels if present"
  (remove-if #'(lambda (x) (add-label node x)) prompt))

(defmethod parse-element ((node tis-node) x)
  "Parses an element of a list as a string and parse to a number, location, or label"
  (cond
    ;; Return plain int's
    ((integerp x) x)
    ;; Convert string ints to ints
    ((every #'digit-char-p x)
      (parse-integer x))
    ;; Parse locations such as ACC and PC
    ((gethash x (t21-locations node))
      (gethash x (t21-locations node)))
    ;; Parse labels
    ((gethash x (t21-labels node))
      (gethash x (t21-labels node)))
    (t (error 'tis-parse-error
         :text (format nil "Element '~a' not found.~%" x)))))

(defmethod parse-prompt ((node tis-node) (prompt list))
  "Turns a prompt into a list that can be placed in *PROG*."
  (cons (first prompt) (map 'list #'(lambda (x) (parse-element node x)) (rest prompt))))
