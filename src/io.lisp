
;;;; I/O from files and from Stdin

(in-package #:tis-99)

;;; I/O
(defun prompt-read (&optional (prompt "->"))
  (format *query-io* "~a " prompt)
  (force-output *query-io*)
  (let ((value (read-line *query-io* nil nil)))
    (if value
      value
      (progn
        (format t "~%")
        "HCF"))))

;;; Error Definitions
(define-condition tis-parse-error (error)
  ((text :initarg :text :reader text)))
