;; tis99.asd

(asdf:defsystem #:TIS-99
  :description "The Defacto Implementation of the TIS-99 Standard"
  :version "0.0.1"
  :author "Jay Kamat <jaygkamat@gmail.com>"
  :licence "GNU GPLv3"
  :serial t
  :components ((:module "src"
                 :: components
                 ((:file "package")
                   (:file "nodes" :depends-on ("accessor"))
                   (:file "t21" :depends-on ("nodes" "accessor" "io"))
                   (:file "pipe")
                   (:file "strings")
                   (:file "io")
                   (:file "accessor")
                   (:file "tis99" :depends-on ("t21" "accessor"))))))

(provide :tis-99)
